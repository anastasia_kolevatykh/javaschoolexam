package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.equals( "" ) || statement.contains( " " )
                || statement.contains( ".." ) || statement.contains( "--" )
                || statement.contains( "++" ) || statement.contains( "//" )
                || statement.contains( "**" ) || statement.contains( "," )) {
            return null;
        }

        int countParentheses = 0;
        for (Character c : statement.toCharArray()) {
            if (c.equals( '(' ) || c.equals( ')' )) {
                countParentheses++;
            }
        }
        if (countParentheses % 2 != 0) {
            return null;
        }

        String result;
        ArrayList<String> expression = getExpression( statement );
        result = String.valueOf( getCalc( expression ) );

        if (getCalc( expression ) == 0.0) {
            return null;
        }

        char lastSymbol = result.charAt( result.length() - 1 );
        while (lastSymbol == '0') {
            result = result.substring( 0, result.length() - 1 );
            lastSymbol = result.charAt( result.length() - 1 );
        }
        lastSymbol = result.charAt( result.length() - 1 );
        if (lastSymbol == '.') {
            result = result.substring( 0, result.length() - 1 );
        }

        return result;
    }

    private double getCalc(ArrayList<String> expression) {
        Stack<Double> calc = new Stack<>();
        String delims = "()*/+-";
        for (String curr : expression) {
            double a;
            if (!delims.contains( curr )) {
                a = Double.parseDouble( curr );
                calc.push( a );
            } else {
                double b, c, res;
                if (curr.equals( "*" )) {
                    b = calc.pop();
                    c = calc.pop();
                    res = c * b;
                    calc.push( res );
                }
                if (curr.equals( "/" )) {
                    b = calc.pop();
                    c = calc.pop();
                    if (b == 0.0) {
                        return 0.0;
                    }
                    res = c / b;
                    calc.push( res );
                }
                if (curr.equals( "-" )) {
                    b = calc.pop();
                    c = calc.pop();
                    res = c - b;
                    calc.push( res );
                }
                if (curr.equals( "+" )) {
                    b = calc.pop();
                    c = calc.pop();
                    res = c + b;
                    calc.push( res );
                }
            }
        }
        return calc.pop();
    }

    private ArrayList<String> getExpression(String statement) {
        String delims = "()*/+-";
        char[] expression = statement.toCharArray();
        ArrayList<String> result = new ArrayList<>();

        int count = 0;
        for (int i = 0; i < statement.length(); i++) {
            String symb = String.valueOf( expression[i] );
            if (delims.contains( symb )) {
                if (result.size() == count) {
                    result.add( symb );
                } else {
                    ++count;
                    result.add( symb );
                }
                count++;
            } else {
                if (result.size() == count) {
                    result.add( symb );
                } else {
                    String temp = result.get( count ) + symb;
                    result.remove( count );
                    result.add( temp );
                }
            }
        }

        ArrayList<String> reversePol = new ArrayList<>();
        Stack<String> oStack = new Stack<>();

        for (String s : result) {
            if (!delims.contains( s )) {
                reversePol.add( s );
                continue;
            }
            if (s.equals( "(" )) {
                oStack.push( "(" );
            } else if (s.equals( ")" )) {
                String op = oStack.pop();
                while (!op.equals( "(" )) {
                    reversePol.add( op );
                    op = oStack.pop();
                }
            } else {
                if (oStack.size() > 0) {
                    if (GetPriority( s ) <= GetPriority( oStack.peek() )) {
                        reversePol.add( oStack.pop() );
                    }
                }
                oStack.push( s );
            }
        }
        while (oStack.size() > 0) {
            reversePol.add( oStack.pop() );
        }

        return reversePol;
    }

    private int GetPriority(String s) {
        switch (s) {
            case "(":
                return 0;
            case ")":
                return 1;
            case "+":
                return 2;
            case "-":
                return 3;
            case "*":
            case "/":
                return 4;
            default:
                return 5;
        }
    }

}
