package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.Stack;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        if (x.isEmpty()) {
            return true;
        }
        if (y.isEmpty()) {
            return false;
        }

        Stack<String> strings = new Stack<>();

        for (int i = x.size() - 1; i >= 0; i--) {
            strings.push( x.get( i ).toString() );
        }

        for (int i = 0; i < y.size(); i++) {
            if ((y.get( i )).toString().equals( strings.peek() )) {
                strings.pop();
            }
            if (strings.empty()){
                return true;
            }
        }

        return strings.empty();
    }
}
