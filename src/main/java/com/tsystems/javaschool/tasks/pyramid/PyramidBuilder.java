package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains( null ) || (inputNumbers.size() >= Integer.MAX_VALUE / 2)) {
            throw new CannotBuildPyramidException();
        }
        inputNumbers.sort( Comparator.naturalOrder() );

        int rowLength = 1;
        int position = 0;
        boolean isWork = true;

        List<ArrayList<Integer>> rows = new ArrayList<>();

        while (isWork) {
            ArrayList<Integer> sequence = new ArrayList<>();

            for (int i = position; ((i < rowLength + position) && (i < inputNumbers.size())); i++) {
                sequence.add( inputNumbers.get( i ) );
            }

            if (inputNumbers.size() == (rowLength + position)) {
                isWork = false;
            } else if (inputNumbers.size() < (rowLength + position)) {
                throw new CannotBuildPyramidException();
            } else {
                position += rowLength;
                ++rowLength;
            }

            rows.add( sequence );
        }

        return getResult( rows, rowLength );
    }

    private int[][] getResult(List<ArrayList<Integer>> rows, int rowLength) {
        int[][] result = new int[rows.size()][rowLength * 2 - 1];

        int spaceCounter = 0;

        for (int h = rows.size() - 1; h >= 0; h--) {
            int resultW = spaceCounter;

            for (int w = 0; w < rows.get( h ).size(); w++) {
                result[h][resultW++] = rows.get( h ).get( w );

                if (w != rows.get( h ).size() - 1) {
                    result[h][resultW++] = 0;
                }
            }

            spaceCounter++;
        }

        return result;
    }


}
